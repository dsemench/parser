#include <iostream>
#include <vector>
#include <string>
#include <regex>
#include <algorithm>
#include <cstdlib>
#include <cstring>

using namespace std;

int		ft_condition(string str) {

	if (str == "<")
		return 1;
	else if (str == ">")
		return 2;
	else if (str == "<=")
		return 3;
	else if (str == ">=")
		return 4;
	else if (str == "==")
		return 5;
	else if (str == "!=")
		return 6;
	else if (str == "&&")
		return 7;
	else if (str == "||")
		return 8;
	else
		return 0;
}

char	ft_comper(int32_t arg_1, int32_t cond, int32_t arg_2) {
	switch (cond) {
		case 1:
			return (arg_1 < arg_2) ? '1' : '0';
		case 2:
			return (arg_1 > arg_2) ? '1' : '0';
		case 3:
			return (arg_1 <= arg_2) ? '1' : '0';
		case 4:
			return (arg_1 >= arg_2) ? '1' : '0';
		case 5:
			return (arg_1 == arg_2) ? '1' : '0';
		case 6:
			return (arg_1 != arg_2) ? '1' : '0';
		case 7:
			return (arg_1 && arg_2) ? '1' : '0';
		case 8:
			return (arg_1 || arg_2) ? '1' : '0';
		default :
			return '0';
	}
}

int32_t	find_operator(char *comper, const char *operat) {
	for (int32_t i = 0; comper[i]; i++) {
		if (comper[i] == operat[0]) {
			int32_t j = i, k = 0;
			while ((comper[j] && operat[k]) && (comper[j] == operat[k])) {
				k++;
				j++;
			}
			if (!operat[k] && comper[j] != '=') {
				return i;
			}
		}
	}
	return -1;
}

void	ft_priority(char *comper, string &res) {
	vector<string>	vec_condotions ( { "<", ">", "<=", ">=", "==", "!=", "&&", "||" } );
	int32_t			pos_str = 0, pos_end = 0, p;


	for (int32_t i = 0; i < 8; i++) {
		if ((p = find_operator(comper, vec_condotions[i].c_str())) >= 0) {
			res = comper;
			res.insert(res.begin(), '(');
			res.insert(res.end(), ')');

			pos_str = p - 1;
			pos_end = p + vec_condotions[i].size() + 1;
			while (res[pos_str] && res[pos_str] >= '0' && res[pos_str] <= '9')
				pos_str--;
			while (res[pos_end] && res[pos_end] >= '0' && res[pos_end] <= '9')
				pos_end++;
			pos_str++;
			pos_end++;
			res.insert(res.begin() + pos_str, '(');
			res.insert(res.begin() + pos_end, ')');
			break ;
		}
	}
}

bool	check(char *comper, string &res) {
	cmatch		pars;
	regex		regular ("([\\d]*)" "([ !=|<>&]*)" "([\\d]*)");

	if (regex_match(comper, pars, regular)) {
		string			cnd = pars[2];
		vector<int32_t>	v;
		string			str_int = pars[1];

		v.push_back(atoi(str_int.c_str()));
		str_int = pars[3];
		v.push_back(atoi(str_int.c_str()));
		res += ft_comper(v[0], ft_condition(cnd), v[1]);
		return true;
	}
	else {
		ft_priority(comper, res);
		return false;
	}
}

void	full_expression(int32_t &i, string &str, vector<int32_t> &vec) {
	int32_t	v_elem;
	string	s_buff;

	if (!vec.empty())
		v_elem = vec[vec.size() - 1];
	else
		v_elem = 1;
	int32_t	size = i - v_elem;
	char	buff[size + 1];

	str.copy(buff, size, v_elem);
	buff[size] = '\0';
	str.erase((str.begin() + v_elem - 1), str.begin() + i + 1);
	if (!check(buff, s_buff)) {
		i = -1;
		vec.clear();
	}
	else {
		i = v_elem - 1;
		vec.pop_back();
	}
	str.insert(v_elem - 1, s_buff.c_str(), s_buff.size());
}

int		main(int argc, char **argv) {
	vector<int32_t> vec;

	if (argc == 2) {
		string str (argv[1]);
		str.erase(remove(str.begin(), str.end(), ' '), str.end());

		for (int32_t i = 0; !str.empty() && str[i]; i++) {
			if (str[i] == '(') {
				vec.push_back(i + 1);
			}
			else if (str[i] == ')') {
				full_expression(i, str, vec);
			}
		}
		if (str == "1" || str == "0"){
			cout << ((str == "1" || str == "(1)") ? "true" : "fasle") << endl;
		}
		else
			cout << "something wrong" << endl;
	}
	return 0;
}
