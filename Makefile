NAME = parser

FLAGS = -Wall -Wextra -Werror -std=c++11

SRC = parser.cpp

OB = $(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OB)
			clang++ $(OB) $(FLAGS) -o $(NAME)

%.o: %.cpp
			clang++ -c -o $@ $< $(FLAGS)

clean:
			rm -f $(OB)

fclean: clean
			rm -f $(NAME)

re: clean all